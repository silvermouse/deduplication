#!/usr/bin/perl

###########################################################################################################
##  Script      : dedupe.pl
##  Author      : James Lawrie (james@silvermouse.net)
##  Date        : 24/05/2019
##  Description : Script to take a list of files and deduplicate them based on filename and checksum
##  License:
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.
###########################################################################################################

use warnings;
use strict;

use Cwd;
use File::Basename;
use File::Copy;
use Digest::MD5 qw(md5 md5_hex md5_base64);

# For processing command-line arguments
use Getopt::Std;
my %options =();
getopts("t:vVd", \%options);                       # The : means it takes a string. -t $to_directory -v verbose -d dry-run

my $dest_dir        = $options{'t'} || getcwd();  # where to dedupe to. If not provided it will overwrite the existing directory. Otherwise it will copy.
my $pool            = "pool/";                    # where to store the actual files. Will include the first three dirs. Eg. 145/10/10395/pool/
my $verbose         = defined($options{'v'}) || defined($options{'V'});
my $superverbose    = defined($options{'V'});
my $dryrun          = defined($options{'d'});

if ($dryrun) {
    print "DRY RUN: No files will be changed. Verbose automatically enabled.\n";
    $verbose = 1;
}

foreach (<>) { # List of filenames. Full or relative path, taken from STDIN.
    chomp(my $filename = $_);
    next if (-d $filename);
    if ($filename =~ /$pool/) {
        print "Skipping $filename because it contains the pool: ${pool}\n" if $verbose;
        next;
    }
    my $checksum = get_checksum_of_file($filename); # split into a function here so we can use different checksums in future.
    if ($checksum) { write_file_to_disk($filename, $checksum) };
}

sub get_checksum_of_file {
    my $filename = shift;
    # If it's already a symlink to the pool we'll just return the checksum from that file. This is purely for efficiency.
    my $dest = readlink($filename);
    if (($dest) && (index($dest, $pool) != -1)) {
        if ($dest =~ /_([a-z0-9]+)$/) {
            return $1;
        }
    }
    if (open my $file, "<", $filename) {
        my $md5 = Digest::MD5->new;
        $md5->addfile($file);
        my $checksum = $md5->hexdigest;
        close $file;
        return $checksum;
    }
    else {
        warn("Unable to open $filename\n");
        return 0;
    }
}

sub write_file_to_disk {
    my ($filename, $checksum) = @_;
    my $dedupe = write_file_to_pool($filename, $checksum, ${dest_dir}, ${pool});
    my $basedir = $filename;
    if ($basedir =~ /(.*\/)[^\/]+/) {
        $basedir = $1;
    }
    print "mkdir -p '${dest_dir}/${basedir}'\n" if $superverbose;
    `mkdir -p "${dest_dir}/${basedir}"` unless $dryrun;
    my $dest_file = $dest_dir ? ${dest_dir} . "/" . $filename : $filename;
    print "Removing $dest_file ready for replacement\n" if $superverbose;
    unlink($dest_file) unless $dryrun;
    $dedupe = generate_relative_link($dedupe, $dest_file);
    my $old_dir = getcwd();
    chdir(dirname($dest_file));
    print "$dest_file -> $dedupe\n" if $verbose;
    if (!$dryrun) { symlink($dedupe, $dest_file) or die($!); }
    chdir($old_dir);
}

sub write_file_to_pool {
    my ($src_file, $checksum, $destdir, $pool) = @_;
    my $filename = $src_file;
    $filename =~ s/.*\/([^\/]+)/$1/; # pulling out the filename. Could use File::Basename for this.
    $filename =~ s/\d{4}-\d\d-\d\d\s\d\d-\d\d-\d\d\.[A-Z0-9\-]+\.//; # removing eg. "2015-08-24 18-53-42.80D65D58-9CDA-20DF-8E2C-7062D78A3BAB"
    $filename .= "_${checksum}";
    my $basedir;
    if ($src_file =~ /^(?:\.\/)?(\d+\/\d+\/\d+\/)/) {
        $basedir = $1;
    }
    my $pool_dir = substr($checksum, 0, 3);
    my $path = $destdir . "/" . $basedir . $pool . $pool_dir;
    `mkdir -p $path` unless $dryrun;
    $path .= "/${filename}";
    print "Copying $src_file to $path" if $superverbose;
    if (!-e $path) {
        if (!$dryrun) {
            copy($src_file, $path) or die "Copy failed: $!";
        }
    }
    else {
        print "$path already exists, skipping" if $superverbose;
    }
    return $path;
}

sub generate_relative_link {
    # Remove any of the path thats the same and then ../
    # /a/b/c/d/ /a/b/e/f/ -> ../../e/f/
    my ($from, $to) = @_;
    $to =~ s/\/\.\//\//g;
    $from =~ s/\/\.\//\//g;
    my @from = split(/\//, $from);
    my @tmp_from = @from;
    my @to = split(/\//, $to);
    my @tmp_to = @to;
    my $link = "";
    for my $i (0 .. $#from) {
        if ($from[$i] eq $to[$i]) {
            shift @tmp_from;
            shift @tmp_to;
        }
        else {
            last;
        }
    }
    foreach (@tmp_to) {
        $link .= "../";
    } $link =~ s/\.\.\///;
    $link .= join("/", @tmp_from);
}
